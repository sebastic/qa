# -*- Makefile -*-
# https://qa.debian.org/ makefile
.SUFFIXES:

RELHTMLBASE := ../web
TARGETDIR := $(WMLBASE)/$(RELHTMLBASE)/$(CUR_DIR)

TEMPLATEDIR := /srv/qa.debian.org/wml/templ
TEMPLATES := $(wildcard $(WMLBASE)/templ/*.wml) $(wildcard $(TEMPLATEDIR)/*.wml)

cssfiles := $(wildcard *.css)
destcss := $(patsubst %.css,$(TARGETDIR)/%.css,$(cssfiles))

jsfiles := $(wildcard *.js)
destjs := $(patsubst %.js,$(TARGETDIR)/%.js,$(jsfiles))

allwmlsources := $(wildcard *.wml)
wml2htmlsources := $(filter-out $(wml2phpsources),$(allwmlsources))

phpfiles := $(patsubst %.wml,%.php,$(wml2phpsources)) $(wildcard *.php)
destphp := $(patsubst %.php,$(TARGETDIR)/%.php,$(phpfiles))

htmlfiles := $(patsubst %.wml,%.html,$(wml2htmlsources))
desthtml := $(patsubst %.html,$(TARGETDIR)/%.html,$(htmlfiles))

existing-SUBS := $(shell for dir in $(wildcard $(SUBS)) ''; do test -d $$dir && echo $$dir; done)
existing-SUBS-install := $(addsuffix -install,$(existing-SUBS))
existing-SUBS-clean := $(addsuffix -clean,$(existing-SUBS))
existing-SUBS-cleandest := $(addsuffix -cleandest,$(existing-SUBS))

all: $(htmlfiles) $(phpfiles) $(existing-SUBS)

install: $(destcss) $(desthtml) $(destjs) $(destphp) $(existing-SUBS-install)

%.html: %.wml $(TEMPLATES)
	wml -q -I$(WMLBASE) $(<F) -o $(@F)@g+w

%.php: %.wml $(TEMPLATES)
	wml -q -I$(WMLBASE) $(<F) -o $(@F)@g+w

$(TARGETDIR)/%.css: %.css
	@test -d $(TARGETDIR) || mkdir -p $(TARGETDIR)
	cp -p $(<F) $(TARGETDIR)

$(TARGETDIR)/%.html: %.html
	@test -d $(TARGETDIR) || mkdir -p $(TARGETDIR)
	cp -p $(<F) $(TARGETDIR)

$(TARGETDIR)/%.js: %.js
	@test -d $(TARGETDIR) || mkdir -p $(TARGETDIR)
	cp -p $(<F) $(TARGETDIR)

$(TARGETDIR)/%.php: %.php
	@test -d $(TARGETDIR) || mkdir -p $(TARGETDIR)
	cp -p $(<F) $(TARGETDIR)

$(existing-SUBS):
	-$(MAKE) -C $@

$(existing-SUBS-install):
	-$(MAKE) -C $(patsubst %-install,%,$@) install

clean::
	rm -f *.html *.php *~
clean:: $(existing-SUBS-clean)

$(existing-SUBS-clean):
	-$(MAKE) -C $(patsubst %-clean,%,$@) clean

cleandest::
	rm -f $(TARGETDIR)/*.html $(TARGETDIR)/*.php
cleandest:: $(existing-SUBS-cleandest)

$(existing-SUBS-cleandest):
	-$(MAKE) -C $(patsubst %-cleandest,%,$@) cleandest

.PHONY: all install clean cleandest
.PHONY: $(existing-SUBS) $(existing-SUBS-install) $(existing-SUBS-clean) $(existing-SUBS-cleandest)
