BEGIN;

SET search_path TO apt;

-- distribution table

DELETE FROM apt.distribution;

COPY apt.distribution (archive, suite, distribution) FROM STDIN WITH DELIMITER ' ';
debian stretch oldoldstable
debian stretch-proposed-updates oldoldstable-proposed-updates
debian-security stretch oldoldstable-security
debian stretch-updates oldoldstable-updates
debian-backports stretch-backports oldoldstable-backports
debian-backports stretch-backports-sloppy oldoldstable-backports-sloppy
debian buster oldstable
debian buster-proposed-updates oldstable-proposed-updates
debian-security buster oldstable-security
debian buster-updates oldstable-updates
debian-backports buster-backports oldstable-backports
debian-backports buster-backports-sloppy oldstable-backports-sloppy
debian bullseye stable
debian bullseye-proposed-updates stable-proposed-updates
debian-security bullseye stable-security
debian bullseye-updates stable-updates
debian bullseye-backports stable-backports
debian bookworm testing
debian bookworm-proposed-updates testing-proposed-updates
debian-security bookworm testing-security
debian bookworm-updates testing-updates
debian sid unstable
debian experimental experimental
\.

COMMIT;
