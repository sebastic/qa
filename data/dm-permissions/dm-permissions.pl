#!/usr/bin/perl -w

# Copyright (C) 2012 Bart Martens <bartm@knars.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

# end of DMUA : 24th of November 2012
# https://lists.debian.org/debian-devel-announce/2012/09/msg00008.html

use strict;
use DB_File;
use POSIX qw(strftime);

my $dir= '/etc/ssl/ca-debian';
my $ca_debian = -d $dir ? "--ca-directory=$dir" : '';

my %carnivore;
my $carnivore_db = tie %carnivore, "DB_File", "/srv/qa.debian.org/data/ddpo/results/carnivore.db", O_RDONLY, 0660, $DB_BTREE or die;

sub fingerprint2carnivoreid
{
	my $fingerprint = shift;
	return undef if( not defined $fingerprint );
	$fingerprint =~ /([0-9A-F]{8})$/ or return undef;
	return $carnivore{"key2id:$1"};
}

sub email2carnivoreid
{
	my $email = shift;
	return undef if( not defined $email );
	$email = lc( $email );
	return $carnivore{"email2id:$email"};
}

sub carnivoreid2name
{
	my $carnivoreid = shift;
	return undef if( not defined $carnivoreid );
	my $name = $carnivore{"name:$carnivoreid"};
	return undef if( not defined $name );
	$name =~ s/^(.*):(Ahmed.*)$/$2:$1/;
	$name =~ s/^(bd\@bc-bd\.org|andrew\.ruthven\@catalyst\.net\.nz|Hanska|Oriole|andreas|Jordi|Matthijs Kooijman IT|Dererk|Horms):(.*)$/$2:$1/;
	$name =~ s/:.*//;
	print STDERR "name has no spaces: $name\n" if( $name !~ / / and $name !~ /^(Wookey)$/ );
	return $name;
}

sub fingerprint2name
{
	my $fingerprint = shift;
	return carnivoreid2name( fingerprint2carnivoreid( $fingerprint ) );
}

sub fingerprint2name_checked
{
	my $fingerprint = shift;
	my $name = fingerprint2name( $fingerprint );
	die "no name found for fingerprint $fingerprint" if( not defined $name );
	return $name;
}

my %dm_fingerprint;
my %dm_carnivoreid;

open INPUT, "gpg --no-default-keyring --keyring /srv/qa.debian.org/data/keyrings/keyrings/debian-maintainers.gpg --fingerprint |" or die;
while(<INPUT>)
{
	chomp;

	next if( not /Key fingerprint/i );
	die if( not /Key fingerprint = ([0-9A-F ]+)$/ );
	my $fingerprint = $1;

	$fingerprint =~ s/ //g;

	$dm_fingerprint{$fingerprint} = 1;

	my $carnivoreid = fingerprint2carnivoreid( $fingerprint );

	if( not defined $carnivoreid )
	{
		#print STDERR "DM fingerprint $fingerprint not found in carnivore\n";
		next;
	}

	$dm_carnivoreid{$carnivoreid} = 1;
}
close INPUT;

sub carnivoreid_is_dm
{
	my $carnivoreid = shift;
	return 0 if( not defined $carnivoreid );
	return defined $dm_carnivoreid{$carnivoreid};
}

my %dm_can_upload_package;

my %dm_maintains_package;
my %package_has_dm;

my $package = undef;
my $maintainer = undef;
my $uploaders = undef;

open INPUT, "zcat /srv/mirrors/debian/dists/unstable/*/source/Sources.gz"
	." /srv/mirrors/debian/dists/experimental/*/source/Sources.gz |"
	or die "failed to read Sources files";
while(<INPUT>)
{
	chomp;

	$package = $1 if( /^Package: (.*)/ );
	$maintainer = $1 if( /^Maintainer: (.*)/ );
	$uploaders = $1 if( /^Uploaders: (.*)/ );

	if( /^$/ )
	{
		die if( ! defined $package );
		die if( ! defined $maintainer );

		my $all_maintainers = $maintainer;
		$all_maintainers .= ", $uploaders" if( defined $uploaders );
		$all_maintainers =~ s/Adam C\. Powell, IV/Adam C. Powell IV/;
		$all_maintainers =~ s/Thomas Bushnell, BSG/Thomas Bushnell BSG/;
		$all_maintainers =~ s/TransNexus, Inc/TransNexus Inc/;
		$all_maintainers =~ s/, *,/,/; # pandoc
		$all_maintainers =~ s/John H\. Robinson, IV/John H. Robinson IV/;
		$all_maintainers =~ s/(Natural Language Processing), (Japanese)/$1 $2/;
		$all_maintainers =~ s/, +/,/g;

		foreach my $maintainer ( split /,/, $all_maintainers )
		{
			$maintainer =~ s%(<zumbi\@debian\.o)$%$1rg>%; # bug 709118
			if( $maintainer !~ / <([^<>@]+\@[^<>@]+)> *$/ )
			{
				print STDERR "package maintainer fails regular expression: $package $maintainer\n";
				next;
			}
			my $email = $1;
			my $carnivoreid = email2carnivoreid( $email );

			if( defined $carnivoreid and defined $dm_carnivoreid{$carnivoreid} )
			{
				$dm_maintains_package{$carnivoreid}{$package} = 1;
				$package_has_dm{$package} = 1;
			}
		}

		$package = undef;
		$maintainer = undef;
		$uploaders = undef;
	}
}
close INPUT;

# 13 Oct 2012: 185 162
die "low number(s) of keys: ".scalar(keys(%dm_maintains_package)) #." ".scalar(keys(%dm_can_upload_package))
	if( scalar(keys(%dm_maintains_package)) < 100 ); # or scalar(keys(%dm_can_upload_package)) < 100 );

my %dd_dm_package;
my %dm_dd_package;
my $block = "";

open INPUT, "wget $ca_debian -q -O - https://ftp-master.debian.org/dm.txt |" or die "wget: $!";
while(<INPUT>)
{
	chomp;

	$block .= "$_\n";

	if( /^$/ )
	{
		$block =~ s/\n / /gs;

		my $fingerprint_dm = undef;
		my $allow = undef;

		foreach my $line ( split /\n/, $block )
		{
			$fingerprint_dm = $1 if( $line =~ /^Fingerprint: ([0-9A-F]+)/ );
			$allow = $1 if( $line =~ /^Allow: (.*)/ );
		}

		die if( not defined $fingerprint_dm or not defined $allow );

		#print STDERR "fingerprint $fingerprint_dm is not in DM keyring\n" if( not defined $dm_fingerprint{$fingerprint_dm} );

		$allow =~ s/, /,/g;

		foreach my $allow_entry ( split /,/, $allow )
		{
			$allow_entry =~ /^(\S+) \(([A-F0-9]+)\)$/ or die;
			my $package = $1;
			my $fingerprint_dd = $2;

			my $carnivoreid = fingerprint2carnivoreid( $fingerprint_dm );
			if( not defined $carnivoreid )
			{
				#print STDERR "fingerprint in dm.txt is not known in carnivore: $fingerprint_dm\n";
				next;
			}

			$dm_can_upload_package{$carnivoreid}{$package} = $fingerprint_dd;

			my $carnivoreid_dd = fingerprint2carnivoreid( $fingerprint_dd );
			if( not defined $carnivoreid_dd )
			{
				print STDERR "fingerprint in dm.txt is not known in carnivore: $fingerprint_dd\n";
				next;
			}

			$dd_dm_package{$carnivoreid_dd}{$carnivoreid}{$package} = 1;
			$dm_dd_package{$carnivoreid}{$carnivoreid_dd}{$package} = 1;
		}

		$block = "";
	}
}
close INPUT;

my %count_sort;

foreach my $carnivoreid ( keys %dm_can_upload_package )
{
	$count_sort{ scalar keys %{$dm_can_upload_package{$carnivoreid}} }{$carnivoreid} = 1;
}

my %hit_dm;
my %hit_package;

open OUTPUT, ">dm-dd-package.txt.new" or die;
foreach my $count ( sort {$b <=> $a} keys %count_sort )
{
	foreach my $carnivoreid_dm ( keys %{$count_sort{$count}} )
	{
		print OUTPUT carnivoreid2name( $carnivoreid_dm )."\n";

		#print OUTPUT "  no longer via DMUA=yes :\n";
		#
		#foreach my $package ( sort keys %{$dm_can_upload_package{$carnivoreid_dm}} )
		#{
		#	next if( $dm_can_upload_package{$carnivoreid_dm}{$package} ne "dmua" );
		#	print OUTPUT "    $package\n";
		#
		#	$hit_dm{$carnivoreid_dm} = 1;
		#	$hit_package{$package} = 1;
		#}

		foreach my $carnivoreid_dd ( keys %{$dm_dd_package{$carnivoreid_dm}} )
		{
			print OUTPUT "  granted by ".carnivoreid2name( $carnivoreid_dd )." :\n";

			foreach my $package ( sort keys %{$dm_dd_package{$carnivoreid_dm}{$carnivoreid_dd}} )
			{
				print OUTPUT "    $package";
				print OUTPUT " --> ERROR : not maintainer of this package"
					if( not defined $dm_maintains_package{$carnivoreid_dm}{$package} );
				print OUTPUT "\n";
			}
		}
	}
}
close OUTPUT;

#print "hit_dm = ".keys(%hit_dm)."\n";
#print "hit_package = ".keys(%hit_package)."\n";

my %count_packages_by_dd;
foreach my $carnivoreid_dd ( keys %dd_dm_package )
{
	foreach my $carnivoreid_dm ( keys %{$dd_dm_package{$carnivoreid_dd}} )
	{
		$count_packages_by_dd{$carnivoreid_dd} += scalar keys %{$dd_dm_package{$carnivoreid_dd}{$carnivoreid_dm}};
	}
}
%count_sort = ();
foreach my $carnivoreid_dd ( keys %count_packages_by_dd )
{
	$count_sort{$count_packages_by_dd{$carnivoreid_dd}}{$carnivoreid_dd} = 1;
}

open OUTPUT, ">dd-dm-package.txt.new" or die;
foreach my $count ( sort {$b <=> $a} keys %count_sort )
{
	foreach my $carnivoreid_dd ( keys %{$count_sort{$count}} )
	{
		print OUTPUT carnivoreid2name( $carnivoreid_dd ) . "\n";

		foreach my $carnivoreid_dm ( keys %{$dd_dm_package{$carnivoreid_dd}} )
		{
			print OUTPUT "  " . carnivoreid2name( $carnivoreid_dm ) . "\n";

			foreach my $package ( sort keys %{$dd_dm_package{$carnivoreid_dd}{$carnivoreid_dm}} )
			{
				print OUTPUT "    $package";
				print OUTPUT " --> ERROR : not maintainer of this package"
					if( not defined $dm_maintains_package{$carnivoreid_dm}{$package} );
				print OUTPUT "\n";
			}
		}
	}
}
close OUTPUT;

open OUTPUT, ">dm-without-packages.txt.new" or die;
foreach my $fingerprint ( keys %dm_fingerprint )
{
	my $carnivoreid = fingerprint2carnivoreid( $fingerprint );
	next if( not defined $carnivoreid ); # already reported elsewhere

	print OUTPUT "$fingerprint " . carnivoreid2name( $carnivoreid ) . "\n" if( not defined $dm_can_upload_package{$carnivoreid} );
}
close OUTPUT;

open OUTPUT, ">errors.txt.new" or die;
foreach my $carnivoreid_dm ( keys %dm_dd_package )
{
	foreach my $carnivoreid_dd ( keys %{$dm_dd_package{$carnivoreid_dm}} )
	{

		foreach my $package ( keys %{$dm_dd_package{$carnivoreid_dm}{$carnivoreid_dd}} )
		{
			if( not defined $dm_carnivoreid{$carnivoreid_dm} )
			{
				print OUTPUT carnivoreid2name( $carnivoreid_dd ) . " granted permission to " . carnivoreid2name( $carnivoreid_dm )
				. " for package $package but " . carnivoreid2name( $carnivoreid_dm )
				. " is not in the DM keyring\n"
					if( carnivoreid2name( $carnivoreid_dm ) ne "Sebastian Ramacher" );
			}
			elsif( not defined $dm_maintains_package{$carnivoreid_dm}{$package}
			and not( $package." ".carnivoreid2name( $carnivoreid_dm ) eq "gitmagic Federico Ceratto"
				and strftime( "%Y-%m-%d", gmtime ) lt "2013-01-01" ) # see ITA 673621
			)
			{
				print OUTPUT carnivoreid2name( $carnivoreid_dd ) . " granted permission to " . carnivoreid2name( $carnivoreid_dm )
				. " for package $package but " . carnivoreid2name( $carnivoreid_dm )
				. " is not maintainer of package $package\n";
			}
		}
	}
}
close OUTPUT;

