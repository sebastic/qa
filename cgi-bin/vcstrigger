#!/usr/bin/perl -T

# Copyright (C) 2014-2018 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use warnings;
use strict;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use DBD::Pg;
use JSON;

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

# POSTDATA: {"projects":[{"git_http_url":"https://salsa.debian.org/security-tracker-team/security-tracker.git"},{"git_http_url":"https://salsa.debian.org/postgresql/plr.git"}]}

my $cgi = new CGI;
my $dbh = DBI->connect("dbi:Pg:service=qa;user=guest", '', '',
    { AutoCommit => 0, RaiseError => 1, PrintError => 1});

unless ($cgi->param('POSTDATA')) {
    print header (-status => '405 Method Not Allowed', -allow => 'POST');
    exit;
}

my $postdata = decode_json(scalar $cgi->param('POSTDATA'));
die "POSTDATA has no projects key" unless (exists $postdata->{projects});

foreach my $project (@{$postdata->{projects}}) {
    die "project has no git_http_url key" unless (exists $project->{git_http_url});
    my $git_http_url = $project->{git_http_url};
    my $rows = $dbh->do("UPDATE vcs SET next_scan = round_time(now()) WHERE url = ? AND next_scan > now()",
        undef, $git_http_url);
    if ($rows == 0) {
        print STDERR "vcstrigger: did not find $git_http_url\n";
    }
}

$dbh->commit;

my $status = "200 OK";
print header (-type => 'text/plain', -charset => 'utf-8', -status => $status);
